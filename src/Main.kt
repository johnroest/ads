fun main() {
    val lifoStack = LifoStack<Int>();

    lifoStack.push(1)
    lifoStack.push(2)
    lifoStack.push(3)

    println(lifoStack.pop())
    println(lifoStack.size())
    println(lifoStack.isEmpty())

    // --------------- END LIFO STACK----------------------


}

