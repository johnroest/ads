class FifoQueue<A> {
    private val queue = ArrayList<A>()

    fun enqueue(item: A) {
        queue.add(item)
    }

    fun dequeue(): A? {
        return queue.removeAt(0)
    }

    fun isEmpty(): Boolean {
        return queue.isEmpty()
    }

    fun size(): Int {
        return queue.size
    }
}