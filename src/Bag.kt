class Bag<A> {

    private val bag: MutableList<Bag<A>> = mutableListOf()

    fun add(item: Bag<A>) {
        bag.addLast(item)
    }

    fun isEmpty(): Boolean {
        return bag.size == 0
    }

    fun size(): Int {
        return bag.size
    }

}