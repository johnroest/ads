class LifoStack<A> {

    private val stack = ArrayList<A>()

    fun push(a: A) {
        stack.add(a)
    }

    fun pop(): A {
        return stack.removeAt(stack.lastIndex)
    }

    fun isEmpty(): Boolean {
        return stack.isEmpty()
    }

    fun size(): Int {
        return stack.size
    }

}