data class Node<T>(var item: T, var next: Node<T>? = null)

class LinkedList<T: Any> {
    private var head: Node<T>? = null
    private var tail: Node<T>? = null
    private var size = 0

    fun isEmpty(): Boolean = size == 0

    override fun toString(): String {
        return if(isEmpty()) "Empty list" else "Contains $size elements"
    }

    fun push(item: T) {
        if(isEmpty()) {
            head = Node(item, null)
        } else {
            tail!!.next = Node(item, null)
        }
        size++
    }

    fun append(item: T): LinkedList<T> = apply {
        if(isEmpty()) {
            push(item)
            return this
        }
        val node = Node(item, null)
        tail!!.next = node
        tail = node
        size++
    }

    fun nodeAt(index: Int): Node<T>? {
        var currentNode = head
        var currentIndex = 0
        while(currentNode != null && currentIndex < index) {
            currentNode = currentNode.next
            currentIndex++
        }
        return currentNode
    }

    fun insert(value: T, afterNode: Node<T>): Node<T> {
        if (tail == afterNode) {
            append(value)
            return tail!!
        }

        val newNode = Node(value, afterNode.next)
        afterNode.next = newNode
        size++

        return newNode
    }
}